Welcome to SORRY!

The game is relatively easy to play.  The biggest thing to know is that the board setup looks like this:

 e  e  e  e  b  e  e  e  e  e 
-- -- -- -- -- -- -- -- -- --
 1  2  3  4  5  6  7  8  9 10

The top values that are 'e' indicate an empty space.  As pawns are moved they will be replaces by r, g, y, or b.
These characters indicate whether the pawns are red, green, yellow, or blue respectively.

The bottom number indicates the index of your pawn.  So lets say you are the blue player.  Your pawn is located at index 5.
Therefor, to move the pawn you will be prompted with a question "Which piece would you like to move".
To move the pawn, enter "5" as that is the number of your piece.

The pieces will move into the home array automatically if there is space in the array.
It is six spaces long, so be sure to only move a piece if there is enough room. Otherwise, the pawn will just skip past the entry to the home array and you will have to move around the board again.

For more information on the rules of SORRY! go here: http://www.hasbro.com/common/instruct/Sorry.PDF

ENJOY!