CHANGELOG:

	Fixed just about every feature of the game:
	
		-turns are now handled (although not by the player class as it should be)
			- kept trying to say every player was the last player initialized in our main method
			- I created a couple methods in the game class that are actually handling the turns

		
		- 1 & 2 now is the ONLY WAY to spawn a peice
			- peices properly spawn at 11, 23, 35, and 47
		
		- board now loops properly
		
		- movepiece() now takes in a 'turn' and 'player' parameter
			- (this is due to the player class not working right so I did what I could to work around this)
			
		- better formatting for the board
			- turns are now nice blocks with a couple lines padding individual turns 
			
		- can no longer move any piece! only pieces with your character value can be moved by you!
		
		- erases pawns that are landed on by different colors (and your colors... you can kill your own pawn)
			
	Problems:
	
		- the game class only checks to see if green player has a pawn on the board
			- now it will prompt users with no pawns to move nothing
				-luckily they cant move the wrong piece now
				- due to the player class constantly thinking it's the last initialized player's turn
					- switch around the way players are initialized, for real. whichever is last initialized, itll always be that one
					- thats why it was always yellow too!
		
		- game cannot be won
		
		- Im not sure if players can even get in home.
			- home arrays are not shown anywhere
			
		- probably more, but Im not sure... play it to find out and add!